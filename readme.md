# FizzBuzz++

PHP console application that is executed from the command line. The script should echo the numbers 1 to 500, each number being on a new line. Output required in addition to the number:

- If the number is divisible by 3 the script should echo 'FIZZ'
- if the number is divisible by 5 the script should echo 'BUZZ'.
- If the number is divisible by both 3 and 5 the script should echo 'FIZZBUZZ'.
- If the number is a prime, echo 'FiZZBUZZ++'

Script also log the above output to a file called fizzbuzz.log in folder Log/. Data that may already be in the fizzbuzz.log file should not be overwritten.

## Usage

just open terminal in project folder and use this command

```bash
php ./app.php
```

## License
You are allowed to use it only for interview purposes :)