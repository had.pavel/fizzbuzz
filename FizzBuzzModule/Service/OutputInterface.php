<?php

interface OutputInterface
{
    /**
     * @param string $line
     * @return void
     */
    public function print(string $line): void;

}