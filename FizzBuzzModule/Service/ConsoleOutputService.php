<?php

class ConsoleOutputService implements OutputInterface
{
    /**
     * @param string $line
     * @return void
     */
    function print(string $line): void
    {
        print($line);
        echo PHP_EOL;
    }

}