<?php

require_once "env.php";

class FileOutputService implements OutputInterface
{
    /**
     * @param string $line
     * @return void
     */
    public function print(string $line): void
    {
        $logger = fopen(Env::LOGGER_FOLDER . Env::LOGGER_FILE, "a") or die("Unable to open file!");
        fwrite($logger, $line . PHP_EOL);
        fclose($logger);
    }

}