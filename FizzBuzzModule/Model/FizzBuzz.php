<?php

require_once __DIR__ . "/../Service/OutputInterface.php";

class FizzBuzz
{

    /**
     * @var OutputInterface
     */
    private OutputInterface $consoleOutputService;

    /**
     * @var OutputInterface
     */
    private OutputInterface $fileOutputService;

    /**
     * @param OutputInterface $consoleOutputService
     * @param OutputInterface $fileOutputService
     */
    public function __construct(
        OutputInterface $consoleOutputService,
        OutputInterface $fileOutputService
    )
    {
        $this->consoleOutputService = $consoleOutputService;
        $this->fileOutputService = $fileOutputService;
    }

    /**
     * @param int $number
     * @return void
     */
    public function doMyHomework(int $number): void
    {
        for ($i = 1; $i <= $number; $i++) {
            $this->processFizzBuzzNumber($i);
        }
    }

    /**
     * @param string $line
     * @return void
     */
    private function processFizzBuzzString(string $line): void
    {
        $this->consoleOutputService->print($line);
        $this->fileOutputService->print($line);
    }

    /**
     * @param int $n
     * @return void
     */
    private function processFizzBuzzNumber(int $n): void
    {
        $result = strval($n) . ' ';

        if ($this->isPrime($n)) {
            $result .= 'FiZZBUZZ++';
            $this->processFizzBuzzString($result);
            return;
        }

        if ($n % 3 === 0) {
            $result .= 'FIZZ';
        }

        if ($n % 5 === 0) {
            $result .= 'BUZZ';
        }

        $this->processFizzBuzzString($result);
    }

    /**
     * @param int $number
     * @return bool
     */
    private function isPrime(int $number): bool
    {
        if ($number == 1)
            return 0;
        for ($i = 2; $i <= $number / 2; $i++) {
            if ($number % $i == 0)
                return 0;
        }
        return 1;
    }

}
