#!/usr/bin/php
<?php

require_once "FizzBuzzModule/Model/FizzBuzz.php";
require_once "FizzBuzzModule/Service/ConsoleOutputService.php";
require_once "FizzBuzzModule/Service/FileOutputService.php";

$fizzBuzzHelper = new FizzBuzz(
    new ConsoleOutputService(),
    new FileOutputService()
);

$fizzBuzzHelper->doMyHomework(500);
